/*
    File Name: VendorModel.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package models;

import dtos.VendorDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;

/*
 * VendorModel.java
 *
 * Created on Aug 31, 2015, 3:03 PM
 *  Purpose:    Contains methods for supporting db access for vendor information
 *              Usually consumed by the resoutce class via DTO
 *  Revisions: 
 */
@RequestScoped
public class VendorModel implements Serializable {
    public VendorModel() {
    }

    /*
        NAME: getVendors
        PURPOSE: retrieves vendors from the database Vendors table
        ACCEPTS: param ds - connection to a data source
        RETURNS: an array list of vendors (VendorDTO)
    */
    public ArrayList<VendorDTO> getVendors(DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        String sql = "SELECT * FROM Vendors";
        ArrayList<VendorDTO> vendors = new ArrayList<>();

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    VendorDTO vendor = new VendorDTO();
                    vendor.setVendorno(rs.getInt("VendorNo"));
                    vendor.setEmail(rs.getString("Email"));
                    vendor.setName(rs.getString("Name"));
                    vendor.setAddress1(rs.getString("Address1"));
                    vendor.setCity(rs.getString("City"));
                    vendor.setProvince(rs.getString("Province"));
                    vendor.setPostalcode(rs.getString("PostalCode"));
                    vendor.setPhone(rs.getString("Phone"));
                    vendor.setVendortype(rs.getString("VendorType"));
                    vendors.add(vendor);
                }//end while
            }//end try
            con.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }//end if
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }//end finally
        return vendors;
    }//getVendors 
    
    /*
        NAME: addVendor
        PURPOSE: inserts a new vendor into the Vendors table
        ACCEPTS: param details - VendorDTO values to retrieve (getters) and add to the db
                 param ds - connection to a data source
        RETURNS: vendorno number whether the add was successful
    */
    public int addVendor(VendorDTO details, DataSource ds) {
        String sql = "INSERT INTO Vendors (Email, Name, Address1," + 
                    "City, Province, PostalCode, Phone, VendorType)" +
                    "VALUES (?,?,?,?,?,?,?,?)";
        Connection con = null;
        PreparedStatement pstmt;
        int vendorno = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, details.getEmail());
            pstmt.setString(2, details.getName());
            pstmt.setString(3, details.getAddress1());
            pstmt.setString(4, details.getCity());
            pstmt.setString(5, details.getProvince());
            pstmt.setString(6, details.getPostalcode());
            pstmt.setString(7, details.getPhone());
            pstmt.setString(8, details.getVendortype());
            pstmt.execute();
            
            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                rs.next();
                vendorno = rs.getInt(1);
            }//end try
            con.close();
        } catch (SQLException ex) {
           System.out.println("SQL issue on add: " + ex.getMessage());
        }//end try
        return vendorno;
    }//addEmployee
    
    /*
        NAME: updateVendor
        PURPOSE: updates a vendor that exists in the database
        ACCEPTS: param details - vendorDTO update a specific row in the database
                 param ds - connection to a data source
        RETURNS: an error value as to whether the update was successful
    */
    public int updateVendor(VendorDTO details, DataSource ds) {
        String sql = "UPDATE Vendors SET Email = ?, Name = ?, " +
                "Address1 = ?, City = ?, Province = ?, PostalCode = ?, " +
                "Phone = ?, VendorType = ? WHERE VendorNo = ?";
        Connection con = null;
        PreparedStatement pstmt;
        int numOfRows = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, details.getEmail());
            pstmt.setString(2, details.getName());
            pstmt.setString(3, details.getAddress1());
            pstmt.setString(4, details.getCity());
            pstmt.setString(5, details.getProvince());
            pstmt.setString(6, details.getPostalcode());
            pstmt.setString(7, details.getPhone());
            pstmt.setString(8, details.getVendortype());
            pstmt.setInt(9, details.getVendorno());
            numOfRows = pstmt.executeUpdate();
        }//end try 
        catch(SQLException ex) {
            System.out.println("SQL issue on update: " + ex.getMessage());
        }//end catch
        return numOfRows;
    }//updateEmployee
    public int deleteVendor(int vendorno, DataSource ds) {
        String sql = "DELETE FROM Vendors WHERE vendorno = ?";
        Connection con = null;
        PreparedStatement pstmt;
        int numOfRows = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, vendorno);
            numOfRows = pstmt.executeUpdate();
        }//end try
        catch (SQLException ex) {
            System.out.println("SQL issue on delete: " + ex.getMessage());
        }//end catch
        return numOfRows;
    }//deleteVendor
}//class