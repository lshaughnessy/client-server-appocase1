/*
    File Name: ProductModel.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package models;

import dtos.ProductDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;

/*
 * VendorModel.java
 *
 * Created on Aug 31, 2015, 3:03 PM
 *  Purpose:    Contains methods for supporting db access for vendor information
 *              Usually consumed by the resoutce class via DTO
 *  Revisions: 
 */
@RequestScoped
public class ProductModel implements Serializable {
    public ProductModel() {
    }
    
    /*
        NAME: getProducts()
        PURPOSE: retrieves all the products (and their values) from the Products database
        ACCEPTS: param ds - connection to a data source 
        RETURNS: an array list of products
    */
    public ArrayList<ProductDTO> getProducts(DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        String sql = "SELECT * FROM Products";
        ArrayList<ProductDTO> products = new ArrayList<>();

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    ProductDTO product = new ProductDTO();
                    product.setVendorno(rs.getInt("VendorNo"));
                    product.setVendorsku(rs.getString("VendorSku"));
                    product.setProductname(rs.getString("ProductName"));
                    product.setCostprice(rs.getDouble("CostPrice"));
                    product.setMsrp(rs.getDouble("Msrp"));
                    product.setRop(rs.getInt("Rop"));
                    product.setEoq(rs.getInt("Eoq"));
                    product.setQoh(rs.getInt("Qoh"));
                    product.setQoo(rs.getInt("Qoo"));
                    product.setQrcode(rs.getBytes("QrCode"));
                    product.setQrcodeText(rs.getString("QrCodeTxt"));
                    product.setProductcode(rs.getString("ProductCode"));

                    products.add(product);
                }//end while
            }//end try
            con.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }//end finally
        return products;
    }//ArrayList 
    
    /*
        NAME: getAllProductsForVendor
        PURPOSE: retrieves a list of products pertaining to that vendor number
        ACCEPTS: param vendorno - int value representing the vendor number
                 param ds - connection to a data source
        RETURNS: an array list of products
    */
    public ArrayList<ProductDTO> getAllProductsForVendor(int vendorno, DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        String sql = "SELECT * FROM Products WHERE VendorNo = ?";
        ArrayList<ProductDTO> products = new ArrayList<>();

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, vendorno);
            
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    ProductDTO product = new ProductDTO();
                    product.setVendorno(rs.getInt("VendorNo"));
                    product.setVendorsku(rs.getString("VendorSku"));
                    product.setProductname(rs.getString("ProductName"));
                    product.setCostprice(rs.getDouble("CostPrice"));
                    product.setMsrp(rs.getDouble("Msrp"));
                    product.setRop(rs.getInt("Rop"));
                    product.setEoq(rs.getInt("Eoq"));
                    product.setQoh(rs.getInt("Qoh"));
                    product.setQoo(rs.getInt("Qoo"));
                    product.setQrcode(rs.getBytes("QrCode"));
                    product.setQrcodeText(rs.getString("QrCodeTxt"));
                    product.setProductcode(rs.getString("ProductCode"));

                    products.add(product);
                }//end while
            }//end try
            con.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }//end if
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }//end finally
        return products;
    }//ArrayList
    
    /*
        NAME: addProduct
        PURPOSE: adds a new product to the Products table
        ACCEPTS: param details - ProductDTO variable used to retrieve data from getters
                 and format them into statements to insert into the database
                 param ds - connection to a data source
        RETURNS: an array list of products
    */
    public String addProduct(ProductDTO details, DataSource ds) {
        String sql = "INSERT INTO Products VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection con = null;
        PreparedStatement pstmt;
        String productcode = null;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql, PreparedStatement.NO_GENERATED_KEYS);
            pstmt.setString(1, details.getProductcode());
            pstmt.setInt(2, details.getVendorno());
            pstmt.setString(3, details.getVendorsku());
            pstmt.setString(4, details.getProductname());
            pstmt.setDouble(5, details.getCostprice());
            pstmt.setDouble(6, details.getMsrp());
            pstmt.setInt(7, details.getRop());
            pstmt.setInt(8, details.getEoq());
            pstmt.setInt(9, details.getQoh());
            pstmt.setInt(10, details.getQoo());
            pstmt.setBytes(11, details.getQrcode());
            pstmt.setString(12, details.getQrcodeText());
            
            pstmt.execute();
            productcode = details.getProductcode();
        } catch (SQLException ex) {
           System.out.println("SQL issue on add: " + ex.getMessage());
        }//end try
        finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }//end if
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }//end finally
        
        return productcode;
    }//addEmployee()
    
   /*
        NAME: updateProduct
        PURPOSE: adds a new product to the Products table
        ACCEPTS: param details - ProductDTO update a specific row in the database
                 param ds - connection to a data source
        RETURNS: an array list of products
    */
    public int updateProduct(ProductDTO details, DataSource ds) {
        String sql = "UPDATE Products SET VendorNo = ?, VendorSku = ?, " +
                "ProductName = ?, CostPrice = ?, Msrp = ?, Rop = ?, " +
                "Eoq = ?, Qoh = ?, Qoo = ?, QrCode = ?, " +
                "QrCodeTxt = ? WHERE ProductCode = ?";
        Connection con = null;
        PreparedStatement pstmt;
        int numOfRows = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, details.getVendorno());
            pstmt.setString(2, details.getVendorsku());
            pstmt.setString(3, details.getProductname());
            pstmt.setDouble(4, details.getCostprice());
            pstmt.setDouble(5, details.getMsrp());
            pstmt.setInt(6, details.getRop());
            pstmt.setInt(7, details.getEoq());
            pstmt.setInt(8, details.getQoh());
            pstmt.setInt(9, details.getQoo());
            pstmt.setBytes(10, details.getQrcode());
            pstmt.setString(11, details.getProductcode());

            numOfRows = pstmt.executeUpdate();
        }//end try
        catch(SQLException ex) {
            System.out.println("SQL issue on update: " + ex.getMessage());
        }//end catch
        return numOfRows;
    }//updateEmployee
    
    /*
        NAME: deleteProduct
        PURPOSE: deletes a row from the Products table
        ACCEPTS: param productcode - string to display that the product with the 
                 corresponsing code has been deleted
                 param ds - connection to a data source
        RETURNS: an array list of products
    */
    public int deleteProduct(String productcode, DataSource ds) {
        String sql = "DELETE FROM Products WHERE ProductCode = ?";
        Connection con = null;
        PreparedStatement pstmt;
        int numOfRows = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, productcode);
            numOfRows = pstmt.executeUpdate();
        }//end try
        catch (SQLException ex) {
            System.out.println("SQL issue on delete: " + ex.getMessage());
        }//end catch
        return numOfRows;
    }//deleteProduct
}//class