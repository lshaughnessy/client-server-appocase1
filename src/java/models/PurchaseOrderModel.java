/*
    File Name: PurchaseOrderModel.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package models;

import dtos.PurchaseOrderDTO;
import dtos.PurchaseOrderLineitemDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;

/*
 * VendorModel.java
 *
 * Created on Aug 31, 2015, 3:03 PM
 *  Purpose:    Contains methods for supporting db access for vendor information
 *              Usually consumed by the resoutce class via DTO
 *  Revisions: 
 */
@RequestScoped
public class PurchaseOrderModel implements Serializable {
    public PurchaseOrderModel() {
    }
    
    /*
        NAME: purchaseOrderAdd
        PURPOSE: inserts a new purchase order into the PurchaseOrders table
        ACCEPTS: param details - ProductDTO update a specific row in the database
                 param ds - connection to a data source
        RETURNS: an array list of products
    */
    public int purchaseOrderAdd(PurchaseOrderDTO details, DataSource ds) {
        String sql = "INSERT INTO PurchaseOrders (Vendorno, Amount, PoDate)" + 
                    "VALUES (?,?,?)";
        Connection con = null;
        PreparedStatement pstmt;
        int poNum = 0;
        
        try {
            con = ds.getConnection();
            con.setAutoCommit(false); //needed for trans rollback
            pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, details.getVendorno());
            pstmt.setBigDecimal(2, details.getTotal());
            pstmt.setString(3, details.getPodate());
            pstmt.execute();
            
            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                rs.next();
                poNum = rs.getInt(1);
            }//end try       
            for (PurchaseOrderLineitemDTO item : details.getItems()) {
                if(item.getQty() > 0)
                {
                    sql = "INSERT INTO PurchaseOrderLineItems (PoNumber,Prodcd," +
                            "Qty,Price) VALUES(?,?,?,?)";
                    pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
                    pstmt.setInt(1, poNum);
                    pstmt.setString(2, item.getProductcode());
                    pstmt.setInt(3, item.getQty());
                    pstmt.setBigDecimal(4, item.getPrice());
                    pstmt.execute();
                    
                    try (ResultSet rs = pstmt.getGeneratedKeys()) {
                        rs.next();
                    }//end try
                }//end if
            }//end for
            con.commit();
            con.close();
        } catch (SQLException ex) {
           System.out.println("SQL issue on add: " + ex.getMessage());
           poNum = -1;
           try {
               con.rollback();
           }catch (SQLException sqx) {
               System.out.println("Rollback failed - " + sqx.getMessage());
           }//end catch
        } catch (Exception e) {
               //Handle other errors
               System.out.println("other issue " + e.getMessage());
               poNum = -1;
           } finally {
                //finally block used to close resources
                try {
                 if(con != null) {
                    con.close();
                 }//end if
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
           }//end catch
        }//end finally try
        return poNum;
    }//purchaseOrderAdd()
    
    /*
        NAME: getProductPurchaseOrder
        PURPOSE: retrieves a set of data values from the database table
        ACCEPTS: param ponum - retrieves a po number
                 param ds - connection to a data source
        RETURNS: a new PurchaseOrderDTO db object
    */
    public PurchaseOrderDTO getProductPurchaseOrder(int ponum, DataSource ds) {
        String sql = "SELECT * FROM PurchaseOrders WHERE PoNumber = ?";
        PreparedStatement pstmt;
        Connection con = null;
        PurchaseOrderDTO order = new PurchaseOrderDTO();
        
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, ponum);
            
            try (ResultSet rs = pstmt.executeQuery()) {
                rs.next();
                order.setPonumber(rs.getInt("PoNumber"));
                order.setVendorno(rs.getInt("VendorNo"));
                order.setTotal(rs.getBigDecimal("Amount"));
                order.setPodate(rs.getString("PoDate"));
                
                sql = "SELECT o.*, p.ProductName FROM PurchaseOrderLineItems " +
                        "o JOIN Products p ON o.ProdCd = p.ProductCode " +
                        "WHERE o.PoNumber = ?";
                
                pstmt = con.prepareStatement(sql);
                pstmt.setInt(1, order.getPonumber());
                
                ArrayList<PurchaseOrderLineitemDTO> items = new ArrayList<>();
                try (ResultSet res = pstmt.executeQuery()) {
                    while (res.next()) {
                        PurchaseOrderLineitemDTO item = new PurchaseOrderLineitemDTO();
                        
                        item.setProductcode(res.getString("ProdCd"));
                        item.setProductname(res.getString("ProductName"));
                        item.setQty(res.getInt("Qty"));
                        item.setPrice(res.getBigDecimal("Price"));
                        item.setExt(item.getPrice().multiply(new BigDecimal(item.getQty())));
                        
                        items.add(item);
                    }//end while
                }//end try
                order.setItems(items);
            }//end try
        }//end try
        catch (SQLException ex) {
           System.out.println("SQL issue on add: " + ex.getMessage());
        } catch (Exception e) {
               //Handle other errors
               System.out.println("other issue " + e.getMessage());
           } finally {
                //finally block used to close resources
                try {
                 if(con != null) {
                    con.close();
                 }//end if
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
           }//end catch
        }//end finally try
        return order;          
    }//getProductNo()
    
}//class