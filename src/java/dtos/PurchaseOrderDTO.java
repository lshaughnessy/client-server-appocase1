/*
    File Name: PurchaseOrderDTO.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package dtos;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
/**
 *
 * @author Lexi Shaughnessy
 */
public class PurchaseOrderDTO implements Serializable {
    public PurchaseOrderDTO() {
    }
        //variables
        private int ponumber;
        private int vendorno;
        private ArrayList<PurchaseOrderLineitemDTO> items;
        private BigDecimal total;
        private String podate;

    //getters and setters
    public int getPonumber() {
        return ponumber;
    }

    public void setPonumber(int ponumber) {
        this.ponumber = ponumber;
    }

    public int getVendorno() {
        return vendorno;
    }

    public void setVendorno(int vendorno) {
        this.vendorno = vendorno;
    }

    public ArrayList<PurchaseOrderLineitemDTO> getItems() {
        return items;
    }

    public void setItems(ArrayList<PurchaseOrderLineitemDTO> items) {
        this.items = items;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getPodate() {
        return podate;
    }

    public void setPodate(String podate) {
        this.podate = podate;
    }        
}//class