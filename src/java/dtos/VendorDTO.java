package dtos;

/*
    File Name: VendorDTO.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
import java.io.Serializable;

public class VendorDTO implements Serializable {
    public VendorDTO() {
    }
        //variables
        private int vendorno;
        private String name;
        private String address1;
        private String city;
        private String province;
        private String postalcode;
        private String phone;
        private String vendortype;
        private String email;
        
        //getters and setters
        public int getVendorno() {
            return this.vendorno;
        }
        public void setVendorno(int inValue) {
            this.vendorno = inValue;
        }
        
        public String getName() {
            return this.name;
        }
        public void setName(String inValue) {
            this.name = inValue;
        }
        
        public String getAddress1() {
            return this.address1;
        }
        public void setAddress1(String inValue) {
            this.address1 = inValue;
        }
        
        public String getCity() {
            return this.city;
        }
        public void setCity(String inValue) {
            this.city = inValue;
        }
        
         public String getProvince() {
            return this.province;
        }
        public void setProvince(String inValue) {
            this.province = inValue;
        }
        
        public String getPostalcode() {
            return this.postalcode;
        }
        public void setPostalcode(String inValue) {
            this.postalcode = inValue;
        }
        
        public String getPhone() {
            return this.phone;
        }
        public void setPhone(String inValue) {
            this.phone = inValue;
        }
        
        public String getVendortype() {
            return this.vendortype;
        }
        public void setVendortype(String inValue) {
            this.vendortype = inValue;
        }
        
        public String getEmail() {
            return this.email;
        }
        public void setEmail(String inValue) {
            this.email = inValue;
        }
}//class
