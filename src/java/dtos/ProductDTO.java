/*
    File Name: ProductDTO.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package dtos;
import java.io.Serializable;
/**
 *
 * @author Lexi Shaughnessy
 */
public class ProductDTO implements Serializable {
    public ProductDTO() {
    }
    //variables
        private String productcode;
        private int vendorno;
        private String vendorsku;
        private String productname;
        private double costprice;
        private double msrp;
        private int rop;
        private int eoq;
        private int qoh;
        private int qoo;
        private byte[] qrcode;
        private String qrtext;

    //getters and setters
    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public int getVendorno() {
        return vendorno;
    }

    public void setVendorno(int vendorno) {
        this.vendorno = vendorno;
    }

    public String getVendorsku() {
        return vendorsku;
    }

    public void setVendorsku(String vendorsku) {
        this.vendorsku = vendorsku;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public double getCostprice() {
        return costprice;
    }

    public void setCostprice(double costprice) {
        this.costprice = costprice;
    }

    public double getMsrp() {
        return msrp;
    }

    public void setMsrp(double msrp) {
        this.msrp = msrp;
    }

    public int getRop() {
        return rop;
    }

    public void setRop(int rop) {
        this.rop = rop;
    }

    public int getEoq() {
        return eoq;
    }

    public void setEoq(int eoq) {
        this.eoq = eoq;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getQoo() {
        return qoo;
    }

    public void setQoo(int qoo) {
        this.qoo = qoo;
    }

    public byte[] getQrcode() {
        return qrcode;
    }

    public void setQrcode(byte[] qrcode) {
        this.qrcode = qrcode;
    }  
    
    public String getQrcodeText() {
        return qrtext;
    }

    public void setQrcodeText(String qrCodeText) {
        this.qrtext = qrCodeText;
    }    
}//class

