/*
    File Name: PurchaseOrderLineitemDTO.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package dtos;
import java.io.Serializable;
import java.math.BigDecimal;
/**
 *
 * @author Lexi Shaughnessy
 */
public class PurchaseOrderLineitemDTO implements Serializable {
    public PurchaseOrderLineitemDTO() {
    }
        //variables
        private String productcode;
        private String productname;
        private BigDecimal price;
        private BigDecimal ext;
        private int qty;

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExt() {
        return ext;
    }

    public void setExt(BigDecimal ext) {
        this.ext = ext;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}//class