/*
    File Name: VendorResource.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package resources;

import dtos.VendorDTO;
import java.net.URI;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import models.VendorModel;

/**
 * REST Web Service - Get routine for Vendor
 *
 * @author Evan
 */
@Path("vendor")
@RequestScoped
public class VendorResource {

    @Context
    private UriInfo context;
    
       // resource already defined in Glassfish
    @Resource(lookup = "jdbc/INFO5059DB")
    DataSource ds;

    /**
     * Creates a new instance of VendorResource
     */
    public VendorResource() {
    }

    @GET
    @Produces("application/json")
    public ArrayList<VendorDTO> getVendorsJson() {
        VendorModel model = new VendorModel();
        ArrayList<VendorDTO> vendors = model.getVendors(ds);
        return vendors;
    }//getVendorJson
    
    /**
     * POST method for creating an instance of Vendor
     * @param vendor
     * @return an HTTP response with id of created vendor
     */
    @POST
    @Consumes("application/json")
    public Response createVendorFromJson(VendorDTO vendor) {
        VendorModel model = new VendorModel();
        int id = model.addVendor(vendor, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(id).build();
    }//createVendorFromJson
    
    /**
     * PUT method for updating an instance of Vendor
     * @param vendor
     * @return an HTTP response with # of rows updated.
     */
    @PUT
    @Consumes("application/json")
    public Response updateVendorFromJson(VendorDTO vendor) {
        VendorModel model = new VendorModel();
        int numOfRowsUpdated = model.updateVendor(vendor, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsUpdated).build();
    }//updateVendorFromJson
    
    /**
     * DELETE method for removing an instance of Vendor
     * @param vendorno
     * @return a number representing the # of rows removed
     */
    @DELETE
    @Path("/{vendorno}")
    public Response deleteVendor(@PathParam("vendorno")int vendorno) {
        VendorModel model = new VendorModel();
        int numOfRowsDeleted = model.deleteVendor(vendorno, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsDeleted).build();
    }//deleteVendor
}//class