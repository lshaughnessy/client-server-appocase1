/*
    File Name: ProductResource.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package resources;

import dtos.ProductDTO;
import java.net.URI;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import models.ProductModel;

/**
 * REST Web Service - Get routine for Product
 *
 * @author Evan
 */
@Path("product")
@RequestScoped
public class ProductResource {

    @Context
    private UriInfo context;
    
       // resource already defined in Glassfish
    @Resource(lookup = "jdbc/INFO5059DB")
    DataSource ds;

    /**
     * Creates a new instance of ProductResource
     */
    public ProductResource() {
    }

    @GET
    @Produces("application/json")
    public ArrayList<ProductDTO> getProductsJson() {
        ProductModel model = new ProductModel();
        ArrayList<ProductDTO> products = model.getProducts(ds);
        return products;
    }//getProductsJson
    
    @GET
    @Path("/{vendorno}")
    @Produces("application/json")
    public ArrayList<ProductDTO> getVendorProductsJson(@PathParam("vendorno") int vendorno) {
        ProductModel model = new ProductModel();
        return model.getAllProductsForVendor(vendorno, ds);
    }//getVendorProductsJson
    
    /**
     * POST method for creating an instance of Product
     * @param product
     * @return an HTTP response with id of created product
     */
    @POST
    @Consumes("application/json")
    public Response createProductFromJson(ProductDTO product) {
        ProductModel model = new ProductModel();
        String code = model.addProduct(product, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(code).build();
    }//createProductFromJson
    
    /**
     * PUT method for updating an instance of Product
     * @param product
     * @return an HTTP response with # of rows updated.
     */
    @PUT
    @Consumes("application/json")
    public Response updateProductFromJson(ProductDTO product) {
        ProductModel model = new ProductModel();
        int numOfRowsUpdated = model.updateProduct(product, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsUpdated).build();
    }//updateProductFromJson
    
    /**
     * DELETE method for removing an instance of Product
     * @param productcode
     * @return a number representing the # of rows removed
     */
    @DELETE
    @Path("/{productcode}")
    public Response deleteProduct(@PathParam("productcode")String productcode) {
        ProductModel model = new ProductModel();
        int numOfRowsDeleted = model.deleteProduct(productcode, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsDeleted).build();
    }//deleteProduct
}//class