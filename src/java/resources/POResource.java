/*
    File Name: POResource.java
    Coder: Lexi Shaughnessy
    Student #: 0658874
    Date: October 23, 2015
*/
package resources;

import dtos.PurchaseOrderDTO;
import java.net.URI;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import models.ProductModel;
import models.PurchaseOrderModel;

/**
 * REST Web Service - Get routine for Product
 *
 * @author Evan
 */
@Path("po")
@RequestScoped
public class POResource {

    @Context
    private UriInfo context;
    
       // resource already defined in Glassfish
    @Resource(lookup = "jdbc/INFO5059DB")
    DataSource ds;

    /**
     * Creates a new instance of ProductResource
     */
    public POResource() {
    }
    
    /*
        NAME: createPO()
        PURPOSE: creates a POST request to create a purchase order
        ACCEPTS: param po - PurchaseOrderDTO object
        RETURNS: an instance of a Purchase Order and a generated PO Number
    */
    @POST
    @Consumes("application/json")
    public Response createPO(PurchaseOrderDTO po) {
       PurchaseOrderModel model = new PurchaseOrderModel();
       int poNum = model.purchaseOrderAdd(po, ds);
       URI uri = context.getAbsolutePath();
       return Response.created(uri).entity(poNum).build();
    }//createPO   
}//class